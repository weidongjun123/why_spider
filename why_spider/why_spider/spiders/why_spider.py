import scrapy


class why_spider(scrapy.Spider):
        name = 'why'
        start_urls = [
            'http://www.swgvsm.com/',
            ]

        def parse(self, response):
                for href in response.xpath('//ul/li/a[contains(@href, "html")]/@href').extract():
                    yield response.follow(href, self.parse_why)

		for href in response.xpath('//ul/li/a[contains(@href, "html")]/@href').extract():
                    yield response.follow(href, self.parse)

        def parse_why(self, response):
                yield{
                    'why': response.xpath('//meta[contains(@name, "keywords")]/@content').extract_first(),
                    'answer': response.xpath('//meta[contains(@name, "description")]/@content').extract_first()
                } 
                
                for href in response.xpath('//ul/li/a[contains(@href, "html")]/@href').extract():
                    yield response.follow(href, self.parse_why)
